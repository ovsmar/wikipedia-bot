import telebot
import wikipedia
import re
from telebot import types


# la c'est token 
bot = telebot.TeleBot('xxx')

#déterminer une language
wikipedia.set_lang("fr")

def getwiki(s):
    try:
        ny = wikipedia.page(s)
        # recupere les mille premiers caractères
        wikitext=ny.content[:1000]
        wikimas=wikitext.split('.')
        wikimas = wikimas[:-1]
        wikitext2 = ''
        for x in wikimas:
            if not('==' in x):
                if(len((x.strip()))>3):
                   wikitext2=wikitext2+x+'.'
            else:
                break
        return wikitext2
        #si le bot ne trouve pas le mot
    except Exception as e:
        return "L'encyclopédie n'a aucune information à ce sujet."

#La fonction qui gère la commande /start
@bot.message_handler(commands=["start"])
def start(m, res=False):
    markup=types.ReplyKeyboardMarkup(resize_keyboard=True)
    item1=types.KeyboardButton("ru")
    item2=types.KeyboardButton("fr")
    markup.add(item1)
    markup.add(item2)
    bot.send_message(m.chat.id, "Choisissez la langue",reply_markup=markup)
    bot.send_message(m.chat.id, "Envoyez-moi n'importe quel mot et je le chercherai sur Wikipedia")


@bot.message_handler(content_types=["text"])
def handle_text(message):
    if message.text.strip() == 'ru' :
            wikipedia.set_lang("ru")
            
    elif message.text.strip() == 'fr':
            wikipedia.set_lang("fr")
            
    
    bot.send_message(message.chat.id, getwiki(message.text))

    bot.polling(none_stop=True, interval=0)



